minetest.register_on_chat_message(function(name, message)

    -- Show the message to all player
    local text = "<" .. name .. "> " .. message
    -- minetest.chat_send_all(minetest.colorize('green', text))
    minetest.chat_send_all(text)

    -- Define the sound file path (replace 'example_sound.ogg' with the actual path to your sound file)
    local sound_file = "notification"

    -- Get a list of all connected players
    local players = minetest.get_connected_players()

    -- Play the sound to each player
    for _, player in ipairs(players) do
        minetest.sound_play(sound_file, {to_player = player:get_player_name(), gain = 0.1})
    end

    return true
end)