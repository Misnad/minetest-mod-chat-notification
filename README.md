# Minetest Mod - ChatNotification


**ChatNotification** is a [Minetest](https://www.minetest.net/) mod that enhances the in-game chat experience by playing a notification sound to all players whenever someone sends a chat message. This helps ensure that important messages are not missed by players who may not be actively monitoring the chat.


For installation, see https://content.minetest.net/help/installing/